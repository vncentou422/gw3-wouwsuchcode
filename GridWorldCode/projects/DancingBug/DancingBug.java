import info.gridworld.actor.Bug;

public class DancingBug extends Bug{
    private int steps;
    private int[] danceSteps;
    private int counter;
    public DancingBug(int[] a)
    {
        steps = 0;
        danceSteps = a;
    }
    public void turn ( int x){
	for (int y = 0; y <= x; y++){
	    turn();
	}
    }
    public void act()
    {
	if (counter == danceSteps.length)
	    counter = 0;
        turn (danceSteps[counter]);
	counter++;
	super.act();
    }
}
    
